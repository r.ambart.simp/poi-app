import React, { Component } from "react";

class App extends Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <a href="/poi" className="navbar-brand">
            Hyco App for POI
          </a>
          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <link to={"/poi"} className="nav-link">
                poi
              </link>
            </li>
            <li className="nav-item">
              <link to={"/add"} className="nav-link">
                Add
              </link>
            </li>
          </div>
        </nav>

        <div className="container mt-3">
          <switch>
            <route exact path={["/", "/poi"]} component={PoiList} />
            <route exact path="/add" component={AddPoi} />
            <route path="/poi/:id" component={Poi} />
          </switch>
        </div>
      </div>
    );
  }
}

export default App;