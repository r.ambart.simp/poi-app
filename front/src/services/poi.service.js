import http from "../http-common";

class PoiDataService {
  getAll() {
    return http.get("/poi");
  }

  get(id) {
    return http.get(`/poi/${id}`);
  }

  create(data) {
    return http.post("/poi", data);
  }

  update(id, data) {
    return http.put(`/poi/${id}`, data);
  }

  delete(id) {
    return http.delete(`/poi/${id}`);
  }

  deleteAll() {
    return http.delete(`/poi`);
  }

  findByTitle(title) {
    return http.get(`/poi?title=${title}`);
  }
}

export default new PoiDataService();
