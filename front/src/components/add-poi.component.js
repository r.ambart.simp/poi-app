import React, { Component } from "react";
import PoiDataService from "../services/poi.service";

export default class AddPoi extends Component {
  constructor(props) {
    super(props);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeGeo= this.onChangeGeo.bind(this);
    this.savePoi = this.savePoi.bind(this);
    this.newPoi = this.newPoi.bind(this);

    this.state = {
      id: null,
      title: "",
      description: "", 
      published: false,

      submitted: false
    };
  }

  onChangeName(e) {
    this.setState({
      title: e.target.value
    });
  }

  onChangeGeo(e) {
    this.setState({
      description: e.target.value
    });
  }

  savePoi() {
    var data = {
      title: this.state.title,
      description: this.state.description
    };

    PoiDataService.create(data)
      .then(response => {
        this.setState({
          id: response.data.id,
          title: response.data.name,
          description: response.data.geo,
          published: response.data.published,

          submitted: true
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  newPoi() {
    this.setState({
      id: null,
      name: "",
      geo: "",
      published: false,

      submitted: false
    });
  }
  
    render() {
      return (
        <div className="submit-form">
          {this.state.submitted ? (
            <div>
              <h4>Your POI is successfull!</h4>
              <button className="btn btn-success" onClick={this.newPoi}>
                Add
              </button>
            </div>
          ) : (
            <div>
              <div className="form-group">
                <label htmlFor="name">Name</label>
                <input
                  type="text"
                  className="form-control"
                  id="name"
                  required
                  value={this.state.name}
                  onChange={this.onChangeName}
                  name="name"
                />
              </div>
  
              <div className="form-group">
                <label htmlFor="geo">Geo</label>
                <input
                  type="text"
                  className="form-control"
                  id="geo"
                  required
                  value={this.state.description}
                  onChange={this.onChangeGeo}
                  name="geo"
                />
              </div>
  
              <button onClick={this.savePoi} className="btn btn-success">
                Submit
              </button>
            </div>
          )}
        </div>
      );
    }
  }