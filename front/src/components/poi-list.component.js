import React,{ Component } from "react";
import PoiDataService from "../services/poi.service" ;
import { Link } from "react-router-dom";

export default class PoiList extends Component {
  constructor(props) {
    super(props);
    this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);
    this.retrievePoi
 = this.retrievePoi
.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActivtPoi = this.setActivtPoi.bind(this);
    this.removeAllPoi
 = this.removeAllPoi
.bind(this);
    this.searchTitle = this.searchTitle.bind(this);

    this.state = {
      Poi
    : [],
      currentPoi: null,
      currentIndex: -1,
      searchTitle: "",
    };
  }

  componentDidMount() {
    this.retrievePoi
();
  }

  onChangeSearchTitle(e) {
    const searchTitle = e.target.value;

    this.setState({
      searchTitle: searchTitle
    });
  }

  retrievePoi
() {
PoiDataService.getAll()
      .then(response => {
        this.setState({
          Poi
        : response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  refreshList() {
    this.retrievePoi
();
    this.setState({
      currentPoi: null,
      currentIndex: -1
    });
  }

  setActivtPoi ; index () {
    this.setState({
      currentPoi:tPoi,
      currentIndex: index
    });
  }

  removeAllPoi
() {
PoiDataService.deleteAll()
      .then(response => {
        console.log(response.data);
        this.refreshList();
      })
      .catch(e => {
        console.log(e);
      });
  }
  searchName(){
.findByTitle(this.state.searchName)

      .then(response => {
        this.setState({
          Poi
        : response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    const { searchName, poi, currentPoi, currentIndex } = this.state;

    return (
      <div className="list row">
        <div className="col-md-8">
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Search by name"
              value={searchName}
              onChange={this.onChangeSearchName}
            />
            <div className="input-group-append">
              <button
                className="btn btn-outline-secondary"
                type="button"
                onClick={this.onChangeSearchName}
              >
                Search
              </button>
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <h4>poi List</h4>

          <ul className="list-group">
            {poi &&
              poi.map((Poi, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActivePoi(Poi, index)}
                  key={index}
                >
                  {Poi.name}
                </li>
              ))}
          </ul>

          <button
            className="m-3 btn btn-sm btn-danger"
            onClick={this.removeAllpoi}
          >
            Remove All
          </button>
        </div>
        <div className="col-md-6">
          {currentPoi ? (
            <div>
              <h4>Poi</h4>
              <div>
                <label>
                  <strong>Title:</strong>
                </label>{" "}
                {currentPoi.name}
              </div>
              <div>
                <label>
                  <strong>Geo:</strong>
                </label>{" "}
                {currentPoi.geo}
              </div>
              <div>
                <label>
                  <strong>Status:</strong>
                </label>{" "}
                {currentPoi.published ? "Published" : "Pending"}
              </div>

              <Link
                to={"/poi/" + currentPoi.id}
                className="badge badge-warning"
              >
                Edit
              </Link>
            </div>
          ) : (
            <div>
              <br />
              <p>Please click on a Poi...</p>
            </div>
          )}
        </div>
      </div>
    );
  }
}