import React, { Component } from "react";
import PoiDataService from "../services/Poi.service";

export default class Poi extends Component {
  constructor(props) {
    super(props);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeGeo = this.onChangeGeo.bind(this);
    this.getPoi = this.getPoi.bind(this);
    this.updatePublished = this.updatePublished.bind(this);
    this.updatePoi = this.updatePoi.bind(this);
    this.deletePoi = this.deletePoi.bind(this);

    this.state = {
      currentPoi: {
        id: null,
        name: "",
        geo: "",
        published: false
      },
      message: ""
    };
  }

  componentDidMount() {
    this.getPoi(this.props.match.params.id);
  }

  onChangeName(e) {
    const name = e.target.value;

    this.setState(function(prevState) {
      return {
        currentPoi: {
          ...prevState.currentPoi,
          title: title
        }
      };
    });
  }

  onChangeGeo(e) {
    const geo = e.target.value;
    
    this.setState(prevState => ({
      currentPoi: {
        ...prevState.currentPoi,
        geo: geo
      }
    }));
  }

  getPoi(id) {
    PoiDataService.get(id)
      .then(response => {
        this.setState({
          currentPoi: response.data
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updatePublished(status) {
    var data = {
      id: this.state.currentPoi.id,
      title: this.state.currentPoi.name,
      description: this.state.currentPoi.geo,
      published: status
    };

    PoiDataService.update(this.state.currentPoi.id, data)
      .then(response => {
        this.setState(prevState => ({
          currentPoi: {
            ...prevState.currentPoi,
            published: status
          }
        }));
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  updatePoi() {
    PoiDataService.update(
      this.state.currentPoi.id,
      this.state.currentPoi
    )
      .then(response => {
        console.log(response.data);
        this.setState({
          message: "The Poi was updated successfully!"
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  deletePoi() {    
    PoiDataService.delete(this.state.currentPoi.id)
      .then(response => {
        console.log(response.data);
        this.props.history.push('/Pois')
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    const { currentPoi } = this.state;

    return (
      <div>
        {currentPoi ? (
          <div className="edit-form">
            <h4>Poi</h4>
            <form>
              <div className="form-group">
                <label htmlFor="name">Name</label>
                <input
                  type="text"
                  className="form-control"
                  id="name"
                  value={currentPoi.name}
                  onChange={this.onChangeName}
                />
              </div>
              <div className="form-group">
                <label htmlFor="geo">geo</label>
                <input
                  type="text"
                  className="form-control"
                  id="geo"
                  value={currentPoi.geo}
                  onChange={this.onChangeGeo}
                />
              </div>

              <div className="form-group">
                <label>
                  <strong>Status:</strong>
                </label>
                {currentPoi.published ? "Published" : "Pending"}
              </div>
            </form>

            {currentPoi.published ? (
              <button
                className="badge badge-primary mr-2"
                onClick={() => this.updatePublished(false)}
              >
                UnPublish
              </button>
            ) : (
              <button
                className="badge badge-primary mr-2"
                onClick={() => this.updatePublished(true)}
              >
                Publish
              </button>
            )}

            <button
              className="badge badge-danger mr-2"
              onClick={this.deletePoi}
            >
              Delete
            </button>

            <button
              type="submit"
              className="badge badge-success"
              onClick={this.updatePoi}
            >
              Update
            </button>
            <p>{this.state.message}</p>
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on a Poi...</p>
          </div>
        )}
      </div>
    );
  }
}